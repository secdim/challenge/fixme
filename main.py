import base64


def encode(png_file):
    with open(png_file, 'rb') as binary_file:
        binary_file_data = binary_file.read()
        base64_encoded_data = base64.b64encode(binary_file_data)
        base64_message = base64_encoded_data.decode('utf-8')

        print(base64_message)


def decode(encoded_file):
    with open(encoded_file, 'r') as encoded_file:
        encoded_file_data = encoded_file.read().encode('utf-8')
        # FIXME


decode('email.png.encoded')
